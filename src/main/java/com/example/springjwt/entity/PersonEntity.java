package com.example.springjwt.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@Table(name="person")
@SQLDelete(sql = "UPDATE person SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
public class PersonEntity {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @org.hibernate.annotations.Type(type = "pg-uuid")
    private UUID id;
    private String firstName;
    private String lastName;
    private String mobile;
    private Integer age;
    private boolean deleted = Boolean.FALSE;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "person", orphanRemoval = true, fetch = FetchType.LAZY)
    private AddressEntity address;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "person", orphanRemoval = true, fetch = FetchType.LAZY)
    private AccountEntity account;

}
