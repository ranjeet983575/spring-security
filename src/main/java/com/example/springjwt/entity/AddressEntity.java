package com.example.springjwt.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.UUID;

@Getter
@Setter
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "address")
@SQLDelete(sql = "UPDATE address SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
public class AddressEntity {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @org.hibernate.annotations.Type(type = "pg-uuid")
    private UUID id;
    private String city;
    private String pin;
    private String country;
    private boolean deleted = Boolean.FALSE;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "person_id")
    @JsonIgnore
    private PersonEntity person;

}
