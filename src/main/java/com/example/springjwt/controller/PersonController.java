package com.example.springjwt.controller;

import com.example.springjwt.entity.PersonEntity;
import com.example.springjwt.model.PersonModel;
import com.example.springjwt.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/person")
public class PersonController {

    @Autowired
    private PersonService personService;


    @GetMapping("/hello")
    @PreAuthorize("hasRole('tester') or hasRole('developer')")
    public String hello() {
        return "Hello";
    }


    @PostMapping
    @PreAuthorize("hasRole('tester') or hasRole('developer')")
    public ResponseEntity<PersonEntity> createPerson(@RequestBody PersonModel person) {
        PersonEntity response = personService.createPerson(person);
        if (response != null) {
            return ResponseEntity.ok(response);
        }
        return new ResponseEntity(null, null, HttpStatus.INTERNAL_SERVER_ERROR);
    }


    @PreAuthorize("hasRole('tester') or hasRole('developer')")
    @GetMapping()
    public List<PersonEntity> listPerson() {
        return personService.findAllPerson();
    }


    @PreAuthorize("hasRole('tester') or hasRole('developer')")
    @GetMapping("/{id}")
    public ResponseEntity<PersonEntity> findPerson(@PathVariable UUID id) {
        return ResponseEntity.ok().body(personService.findPersonById(id));
    }


    @PreAuthorize("hasRole('tester') or hasRole('developer')")
    @DeleteMapping("/{id}")
    public String deletePerson(@PathVariable UUID id) {
         personService.deletePerson(id);
         return id.toString();
    }

}
