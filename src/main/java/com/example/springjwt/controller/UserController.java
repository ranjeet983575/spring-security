package com.example.springjwt.controller;

import com.example.springjwt.entity.UserEntity;
import com.example.springjwt.jwt.JwtTokenUtil;
import com.example.springjwt.jwt.JwtUserDetailService;
import com.example.springjwt.jwt.JwtUserDetails;
import com.example.springjwt.jwt.config.AuthenticationException;
import com.example.springjwt.jwt.JwtTokenResponse;
import com.example.springjwt.model.LoginModel;
import com.example.springjwt.model.UserModel;
import com.example.springjwt.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private JwtUserDetailService jwtUserService;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @PostMapping("/signup")
    public ResponseEntity<UserEntity> createUser(@RequestBody UserModel user) {
        UserEntity user1 = userService.createUser(user);
        HttpHeaders responseHeaders = new HttpHeaders();
        return new ResponseEntity(user1, responseHeaders, HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<JwtTokenResponse> getLoginToken(@RequestBody LoginModel model) {
        authenticate(model.getEmail(), model.getPassword());
        final JwtUserDetails userDetails = jwtUserService.loadUserByUsername(model.getEmail());
        final String token = jwtTokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(new JwtTokenResponse(token));
    }

    private void authenticate(String username, String password) {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new AuthenticationException("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new AuthenticationException("INVALID_CREDENTIALS", e);
        }
    }


}
