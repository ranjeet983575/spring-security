package com.example.springjwt.controller;

import com.example.springjwt.entity.PersonEntity;
import com.example.springjwt.model.PersonModel;
import com.example.springjwt.service.PersonService;
import com.example.springjwt.serviceImpl.KafkaProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/kafka")
public class KafkaController {

    @Autowired
    private KafkaProviderService kafkaService;

    @GetMapping("/hello/{message}")
    public String hello(@PathVariable String message) {
        kafkaService.send(message);
        return "Message Sent To Kafka";
    }


}
