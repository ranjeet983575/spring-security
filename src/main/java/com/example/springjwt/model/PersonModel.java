package com.example.springjwt.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonModel {

    private String firstName;
    private String lastName;
    private String mobile;
    private Integer age;
    private String city;
    private String pin;
    private String country;
    private String accountNumber;
    private String accountType;
    private double balance;


}
