package com.example.springjwt.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserModel {

    private String email;
    private String userName;
    private String password;

}
