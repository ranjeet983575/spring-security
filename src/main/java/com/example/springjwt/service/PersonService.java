package com.example.springjwt.service;

import com.example.springjwt.entity.PersonEntity;
import com.example.springjwt.model.PersonModel;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface PersonService {

    public PersonEntity createPerson(PersonModel person);

    public PersonEntity findPersonById(UUID personId);

    public PersonEntity updatePerson(PersonModel person, UUID personId);

    public List<PersonEntity> findAllPerson();

    public UUID deletePerson(UUID personId);


}
