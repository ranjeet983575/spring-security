package com.example.springjwt.service;

import org.springframework.stereotype.Service;

@Service
public interface KafkaProviderService {

    public void send(String message);
}
