package com.example.springjwt.service;

import com.example.springjwt.entity.UserEntity;
import com.example.springjwt.model.UserModel;
import org.apache.catalina.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public interface UserService {

    public UserEntity createUser(UserModel user);

    public UserEntity findByEmail(String email);
}
