package com.example.springjwt.serviceImpl;

import com.example.springjwt.entity.AccountEntity;
import com.example.springjwt.entity.AddressEntity;
import com.example.springjwt.entity.PersonEntity;
import com.example.springjwt.model.PersonModel;
import com.example.springjwt.repository.AddressRepository;
import com.example.springjwt.repository.PersonRepository;
import com.example.springjwt.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonRepository personRepo;

    @Override
    public PersonEntity createPerson(PersonModel personModel) {

        PersonEntity person = new PersonEntity();
        AddressEntity address = new AddressEntity();
        AccountEntity account=new AccountEntity();

        person.setAge(personModel.getAge());
        person.setFirstName(personModel.getFirstName());
        person.setLastName(personModel.getLastName());
        person.setMobile(personModel.getMobile());
        person.setAge(person.getAge());

        address.setCity(personModel.getCity());
        address.setPin(personModel.getPin());
        address.setCountry(personModel.getCountry());
        address.setPerson(person);

        account.setAccountNumber(personModel.getAccountNumber());
        account.setAccountType(personModel.getAccountType());
        account.setBalance(personModel.getBalance());
        account.setPerson(person);

        person.setAddress(address);
        person.setAccount(account);

        return personRepo.save(person);
    }

    @Override
    public PersonEntity findPersonById(UUID personId) {
        Optional<PersonEntity> personOpt = personRepo.findById(personId);
        if (personOpt.isPresent()) {
            return personOpt.get();
        }
        return null;
    }

    @Override
    public PersonEntity updatePerson(PersonModel personModel, UUID personId) {
        Optional<PersonEntity> personOpt = personRepo.findById(personId);
        if (personOpt.isPresent()) {
            PersonEntity person = personOpt.get();
            AddressEntity address = person.getAddress();
            AccountEntity account=new AccountEntity();

            person.setAge(personModel.getAge());
            person.setFirstName(personModel.getFirstName());
            person.setLastName(personModel.getFirstName());
            person.setMobile(personModel.getMobile());

            address.setCity(personModel.getCity());
            address.setPin(personModel.getPin());

            account.setAccountNumber(personModel.getAccountNumber());
            account.setAccountType(personModel.getAccountType());
            account.setBalance(personModel.getBalance());
            account.setPerson(person);

            person.setAddress(address);
            person.setAccount(account);
            return personRepo.save(person);
        }
        return null;
    }

    @Override
    public List<PersonEntity> findAllPerson() {
        return personRepo.findAll();
    }

    @Override
    public UUID deletePerson(UUID personId) {
        personRepo.deleteById(personId);
        return personId;
    }
}
