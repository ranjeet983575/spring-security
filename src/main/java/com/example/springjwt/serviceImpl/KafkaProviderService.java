package com.example.springjwt.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaProviderService implements com.example.springjwt.service.KafkaProviderService {

    @Autowired
    private KafkaTemplate kafkaTemplate;

    String kafkaTopic = "demotopic";

    public void send(String message) {
        kafkaTemplate.send(kafkaTopic, message);
    }

}
