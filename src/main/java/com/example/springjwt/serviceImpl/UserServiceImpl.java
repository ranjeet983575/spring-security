package com.example.springjwt.serviceImpl;

import com.example.springjwt.entity.RoleEntity;
import com.example.springjwt.entity.UserEntity;
import com.example.springjwt.jwt.JwtUserDetails;
import com.example.springjwt.model.UserModel;
import com.example.springjwt.repository.UserRepository;
import com.example.springjwt.repository.UserRoleRepository;
import com.example.springjwt.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private UserRoleRepository roleRepo;

    Set<String> userAuthorities = new HashSet<>();

    @Override
    public UserEntity createUser(UserModel user) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(12);
        RoleEntity role = new RoleEntity();
        role.setRole("developer");
        UserEntity userEntity = new UserEntity();
        userEntity.setEmail(user.getEmail());
        userEntity.setUsername(user.getUserName());
        userEntity.setPassword(encoder.encode(user.getPassword()));
        userEntity.setStatus("created");
        role.setUser(userEntity);
        roleRepo.save(role);
        return userRepo.save(userEntity);
    }

    @Override
    public UserEntity findByEmail(String email) {
        return userRepo.findByEmail(email);
    }


}
