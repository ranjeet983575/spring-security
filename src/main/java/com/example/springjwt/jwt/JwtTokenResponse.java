package com.example.springjwt.jwt;

import java.io.Serializable;

public class JwtTokenResponse implements Serializable {

	private static final long serialVersionUID = 8317676219297719109L;

	private final String token;

	public JwtTokenResponse(String token) {
		super();
		this.token = token;

	}

	public synchronized String getToken() {
		return token;
	}

}
