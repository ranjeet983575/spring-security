package com.example.springjwt.jwt;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.example.springjwt.entity.UserEntity;
import com.example.springjwt.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailService implements UserDetailsService {

    @Autowired
    UserRepository userRepo;

    @Override
    public JwtUserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        UserEntity user = userRepo.findByEmail(username);
        Set<String> userRoles = new HashSet<>();
        if (user == null) {
            throw new UsernameNotFoundException("User 404");
        }
        user.getRoles().forEach(role -> userRoles.add(role.getRole()));
        if (userRoles.isEmpty()) {
            throw new UsernameNotFoundException("User 404");
        }

        List<GrantedAuthority> grantedAuthorities = user.getRoles().stream().map(r -> {
            return new SimpleGrantedAuthority("ROLE_" + r.getRole());
        }).collect(Collectors.toList());


        return new JwtUserDetails(user.getEmail(), user.getPassword(),
                true,
                true, true, true, grantedAuthorities, userRoles);
    }


}
